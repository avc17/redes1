from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel

class UFPRTopo( Topo ):

    def __init__( self, n12 = 30, n3 = 15, n4 = 10, nc3 = 20 ):

        Topo.__init__( self )

        switch_labs = self.addSwitch( 'slab' )
        switch_base = self.addSwitch( 'sbase' )
        switch_c3sl = self.addSwitch( 'sc3sl' )

        for h in range(n12):
            hostl12 = self.addHost('hl12%s' % (h + 1))
            self.addLink(hostl12, switch_labs, bw=10, delay='12ms', loss='2')
        for h in range(n3):
            hostl3 = self.addHost('hl3%s' % (h + 1))
            self.addLink(hostl3, switch_labs bw=10, delay='9ms', loss='3')
        for h in range(n4):
            hostl4 = self.addHost('hl4%s' % (h + 1))
            self.addLink(hostl4, switch_labs bw=10, delay='6ms', loss='4')
        for h in range(nc3):
            hostc3 = self.addHost('hc3%s' % (h + 1))
            self.addLink(hostc3, switch_c3sl bw=100, delay='3ms', loss='1')
            
        self.addLink(switch_c3sl, switch_base bw=100, delay='1ms')
        self.addLink(switch_labs, switch_base bw=100, delay='1ms')


topos = { 'ufprtopo': ( lambda: UFPRTopo() ) }

def perfTest():

    topo = UFPRTopo()
    net = Mininet( topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()
    print "Dumping host connections"
    dumpNodeConnections( net.hosts )
    print "Testing network connectivity"
    net.pingAll()
    print "Testing bandwidth between hc31 and hc34"
    hc1, hc4 = net.get( 'hc31', 'hc34' )
    net.iperf( (h1, h4) )
    print "Testing bandwith between hc310 and hl125"
    hc10, h125 = net.get( 'hc310'. 'hl125')
    net.iperf( (hc10, h125) )
    print "Testing bandwidth between hl49 and hl35"
    h49, h35 = net.get( 'hl49', 'hl35')
    net.iperf( (h49, h35) )
    net.stop()